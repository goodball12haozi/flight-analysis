package net.suncaper.flightanalysis.controller;

import net.suncaper.flightanalysis.domain.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class WorldMapController extends HttpServlet {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @RequestMapping("/search")
    //@ResponseBody
    //你这里没返回页面呀
    //就是不知道怎么返回页面。。。网上的方式差了行不通
    public String WorldMap(Model model, String departureCity, String departureDate)
    {
        String sql = "select arrival_city, arrival_airport_code, min(price) min_price, url from flight where departure_city = '" + departureCity + "' and departure_time like '" + departureDate +"%' group by arrival_city limit 10;";
        List<Map<String, Object>> results = jdbcTemplate.queryForList(sql);

        List<Ticket> tickets = new ArrayList<Ticket>();

        for(int i = 0; i < results.size(); i++)
        {
            Ticket ticket = new Ticket();
            Map<String, Object> map = results.get(i);
            for(String key : map.keySet())
            {
                if(key.equals("arrival_city"))
                {
                    ticket.setArrival_city((String) map.get(key));
                }
                else if(key.equals("arrival_airport_code"))
                {
                    ticket.setArrival_airport_code((String) map.get(key));
                }
                else if(key.equals("min_price"))
                {
                    ticket.setMin_price((Integer) map.get(key));
                }
                else if(key.equals("url"))
                {
                    ticket.setUrl((String) map.get(key));
                }
            }
            tickets.add(ticket);
        }

        model.addAttribute("ticketInfo", tickets);
        System.out.println(departureCity);
        System.out.println(departureDate);
        System.out.println(results);

        for(int i = 0; i < tickets.size(); i++)
        {
            System.out.println(tickets.get(i).getArrival_city());
            System.out.println(tickets.get(i).getArrival_airport_code());
            System.out.println(tickets.get(i).getMin_price());
            System.out.println(tickets.get(i).getUrl());
        }

        return "WorldMap";
    }

    @RequestMapping("/worldMap")
    public String showWorldMap(Model model)
    {
        return "WorldMap";
    }

    /*
    @GetMapping("map")
    //查询调用这个，如果你需要传数据就直接查询返回到页面，可以用map,Model
    public String map(){//接你输入的值
        System.out.println("111");
        Map<String ,String> map =new HashMap<>();
       // 这里查询
        map.put("results","test111");//会吗？就thymeleaf 可以看一下
        //页面可以通过map的key获取到值，是的
        return "WorldMap";
    }*/
}